FROM tomcat:8.0

MAINTAINER 1010101

ADD https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war /usr/local/tomcat/webapps/

RUN mkdir ROOT

COPY index.html /usr/local/tomcat/webapps/ROOT/

USER root

WORKDIR /usr/local/tomcat/webapps

EXPOSE 8080

CMD ["catalina.sh", "run"]
